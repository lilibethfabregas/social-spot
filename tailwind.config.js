module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
        colors: {
            accent: '#A5A6A4',
            main: '#F2F2F2',
            one: '#E5E3DB',
            two: '#D9D8D7',
            gray1: '#D9D8D7',
            gray2: '#A5A6A4',
            three: '#F2CFC2',
            pink: '#F2CFC2',
            violet: '#6969d3',
            grey: '#d8d8d8',
            eleven: '#ccc',
            gradient: '#BFBDB7',
            gold: '#ffd700',
            button: '#ddd',
        },
        fontFamily: {
            'sans': ['Lato'],
            'serif': ['DM Serif Display'],
            'cursive': ['Courgette']
        },
        spacing: {
            'line': '2px',
            '30': '7.5rem',
            '58': '14.5rem',
            '76': '19rem',
            '112': '28rem',
            'card': '32rem',
            '208': '52rem',
        }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
