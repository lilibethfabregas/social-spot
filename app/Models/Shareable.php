<?php

namespace App\Models;

trait Shareable
{
    protected static function bootShareable()
    {
        static::deleting(function ($model) {
            $model->shares->each->delete();
        });
    }

    public function isShared()
    {
        return $this->shares()->where('user_id', auth()->id())->count();
    }

    public function getIsSharedAttribute()
    {
        return $this->isShared();
    }

    public function getSharesCountAttribute()
    {
        return $this->shares()->count();
    }

    public function shares()
    {
        return $this->morphMany(Share::class, 'Shareable');
    }

    public function share()
    {
        $arr = ['user_id' => auth()->id()];
        if (!$this->shares()->where($arr)->exists()) {
            return $this->shares()->create($arr);
        }
    }
}
