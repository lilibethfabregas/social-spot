<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Post extends Model implements HasMedia
{
    use HasFactory, Likeable, Shareable, HasMediaTrait;

    protected $fillable = [
        'user_id',
        'title',
        'body',
        'is_draft',
    ];

    protected $casts = [
        'is_draft' => 'boolean'
    ];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('post')
            ->useFallbackUrl('https://i.pinimg.com/originals/61/41/b2/6141b27ca84980229fa81104738df3ae.jpg');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function isOwner()
    {
        return $this->user_id === auth('web')->id();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class)->with('user')->latest();
    }

    public function addComment($comment)
    {
        return $this->comments()->create($comment);
    }

    public function likes()
    {
        return $this->hasMany(Like::class, 'likeable_id');
    }

    public function shares()
    {
        return $this->hasMany(Share::class, 'shareable_id');
    }
}
