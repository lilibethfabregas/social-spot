<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class User extends Authenticatable implements HasMedia
{
    use HasFactory, Notifiable, HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $with = ['userProfile'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    protected static function boot()
    {
        parent::boot();

        static::created(function ($user) {
            $user->createProfile();

            $friends = User::all();

            $friends->each(function ($friend) use ($user) {
                if ($friend->id != $user->id) {
                    $user->addFriend($friend);
                }
            });
        });

        static::deleted(function ($user) {
            $user->deleteFriend($user->id);
        });
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('profile_photo')
            ->singleFile()
            ->useFallbackUrl('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT9ZO94NfyZ-lf9wnfNfOVYkUMUIhYnxeZm6iDD6rc9FYJqe19tZ_5e-8aGBv1jK8fppWc&usqp=CAU');

        $this->addMediaCollection('banner')
            ->singleFile()
            ->useFallbackUrl('https://img.freepik.com/free-photo/white-luxury-fabric-background-with-copy-space_46250-2192.jpg?size=626&ext=jpg');
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function userProfile()
    {
        return $this->hasOne(Profile::class);
    }

    public function getUserProfileAttribute()
    {
        return $this->userProfile();
    }

    public function createProfile()
    {
        return $this->profile()->create(['user_id' => $this->id]);
    }

    public function friends()
    {
        return $this->belongsToMany(User::class, 'friends', 'user_id', 'friend_id')
            ->withPivot('is_confirmed', 'is_blocked')
            ->withTimestamps();
    }

    public function addFriend($friend)
    {
        $this->friends()->sync($friend, false);
    }

    public function deleteFriend($friend)
    {
        $this->friends()->detach($friend, true);
    }

    public function posts()
    {
        return $this->hasMany(Post::class)->latest();
    }

    public function shares()
    {
        return $this->hasMany(Share::class)->latest();
    }
}
