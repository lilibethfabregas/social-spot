<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'shareable_id', 'shareable_type'];

    public function shared()
    {
        return $this->morphTo();
    }
}
