<?php

namespace App\Models;

trait Likeable
{
    protected static function bootLikeable()
    {
        static::deleting(function ($model) {
            $model->likes->each->delete();
        });
    }

    public function isLiked()
    {
        return $this->likes()->where('user_id', auth()->id())->exists();
    }

    public function getIsLikedAttribute()
    {
        return $this->isLiked();
    }

    public function getLikesCountAttribute()
    {
        return $this->likes()->count();
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function like()
    {
        $arr = ['user_id' => auth()->id()];
        if (!$this->likes()->where($arr)->exists()) {
            return $this->likes()->create($arr);
        }
    }

    public function unlike()
    {
        $arr = ['user_id' => auth()->id()];

        $this->likes()->where($arr)->get()->each->delete();
    }
}
