<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class FriendController extends Controller
{
    public function unfriend($userSlug)
    {
        $friend = User::where('slug', $userSlug)->first();

        $user = auth('web')->user();
        $user->deleteFriend($friend);

        session()->flash('success', $friend->name . ' has been unfriended.');
    }
}
