<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Post $post
     * @param Request $request
     * @return void
     */
    public function store(Post $post, Request $request)
    {
        $post->addComment([
            'body' => request('body'),
            'user_id' => auth('web')->id(),
        ]);
    }
}
