<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StorePostRequest;
use App\Http\Transformers\PostTransformer;
use App\Models\Post;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;

class PostController extends Controller
{
    public function get()
    {
        $user = User::find((int)session()->get('userId'));

        $shared = $user->shares;

        $sharedPosts = $shared->map(function ($post) {
            return Post::query()
                ->whereKey($post->shareable_id)
                ->first();
        });

        $ownPosts = Post::query()
            ->with('user')
            ->withCount(['comments', 'likes', 'shares'])
            ->where('user_id', $user->id)
            ->latest()
            ->get();

        $posts = $ownPosts->merge($sharedPosts);

        return $this->response->withCollection($posts, new PostTransformer());
    }

    public function getAll()
    {
        $posts = Post::query()->latest()->get();

        return $this->response->withCollection($posts, new PostTransformer());
    }

    public function getOne()
    {
        $id = (int)session()->get('postId');
        $posts = Post::query()->where('id', $id)->get();

        return $this->response->withCollection($posts, new PostTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePostRequest $request
     * @return void
     */
    public function store(StorePostRequest $request)
    {
        Post::create([
            'user_id' => auth('web')->id(),
            'title' => $request->title,
            'body' => $request->body,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StorePostRequest $request
     * @param Post $post
     * @return JsonResponse
     */
    public function update(StorePostRequest $request, Post $post)
    {
        if (! $post->isOwner()) {
            return response()->json(['message' => 'You are not allowed to update other user\'s post.'], 200);
        }

        $post->update([
            'title' => $request->title,
            'body' => $request->body,
        ]);

        return response()->json(['message' => 'Successfully updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Post $post)
    {
        if (! $post->isOwner()) {
            return response()->json(['message' => 'You are not allowed to delete other user\'s post.'], 200);
        }

        $post->delete();

        return response()->json(['message' => 'Successfully deleted. Redirecting to home.']);
    }

    public function like(Post $post)
    {
        $post->isLiked() ? $post->unlike() : $post->like();

        $message = $post->isLiked() ? 'Post has been unliked.' : 'Post has been liked.';

        return response()->json([
            'likes' => $post->likes()->count(),
            'message' => $message
        ]);
    }

    public function share(Post $post)
    {
        $post->share();
    }
}
