<?php

namespace App\Http\Controllers\Api;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class ReplyController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @param Comment $comment
     * @return Builder[]|Collection
     */
    public function __invoke(Comment $comment)
    {
        return $comment->replies;
    }
}
