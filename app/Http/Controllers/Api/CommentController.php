<?php

namespace App\Http\Controllers\Api;

use App\Models\Post;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class CommentController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @param Post $post
     * @return Builder[]|Collection
     */
    public function __invoke(Post $post)
    {
        return $post->comments;
    }
}
