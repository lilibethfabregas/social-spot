<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Contracts\Support\Renderable;

class PostController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $posts = Post::query()
            ->with('user')
            ->withCount(['comments', 'likes', 'shares'])
            ->latest()
            ->get();

        return view('home', compact('posts'));
    }

    public function show(Post $post)
    {
        session()->put('postId', $post->id);


        return view('posts.show', compact('post'));
    }

    public function share(Post $post)
    {
        $post->share();
    }

    public function shared(Post $post)
    {
        return $post->isShared();
    }
}
