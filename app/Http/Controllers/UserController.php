<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $friends = auth('web')->user()->friends;

        return view('users.index', compact('friends'));
    }

    public function show(User $user)
    {
        session()->put('userId', $user->id);
        $profile = $user->profile;

        return view('users.show', compact('user', 'profile'));
    }

    public function update(Request $request)
    {
        $user = auth('web')->user();
        $user->addMedia($request->file('profile_photo'))->toMediaCollection('profile_photo');
        return back();
    }

    public function addBanner(Request $request)
    {
        $user = auth('web')->user();
        $user->addMedia($request->file('banner'))->toMediaCollection('banner');
        return back();
    }
}
