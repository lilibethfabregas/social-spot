<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function edit()
    {
        return view('users.edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $profileId
     * @return RedirectResponse
     */
    public function upload(Request $request, $profileId)
    {
        $image = $request->file('image');
        $image_name = time() . "." . $image->getClientOriginalExtension();
        $destination = "images/";
        $image->move($destination, $image_name);

        $profile = Profile::find($profileId);

        $profile->update([
            'image' => $destination . $image_name,
        ]);

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Profile $profile
     * @return RedirectResponse
     */
    public function update(Request $request, Profile $profile)
    {
        $profile->update([
            'nickname' => $request->nickname,
            'birthday' => $request->birthday,
            'city' => $request->city,
            'province' => $request->province,
            'mobile' => $request->mobile,
        ]);
    }
}
