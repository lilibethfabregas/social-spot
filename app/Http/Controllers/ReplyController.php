<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ReplyController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Comment $comment
     * @param Request $request
     * @return void
     */
    public function store(Comment $comment, Request $request)
    {
        $comment->addReply([
            'body' => request('body'),
            'user_id' => auth('web')->id(),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param $commentId
     * @return Response
     */
    public function show($commentId)
    {
        $comment = Comment::find($commentId);
        return $comment->replies;
    }
}
