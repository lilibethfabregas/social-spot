<?php

namespace App\Http\Transformers;

use App\Models\Post;
use League\Fractal\TransformerAbstract;

class PostTransformer extends TransformerAbstract
{
    public function transform(Post $post)
    {
        return [
            'id' => $post->id,
            'title' => $post->title,
            'image' => $post->getFirstMediaUrl('post'),
            'body' => $post->body,
            'isDraft' => $post->is_draft,
            'owner' => $post->user->name,
            'ownerSlug' => $post->user->slug,
            'isOwner' => $post->isOwner(),
            'isShared' => $post->isShared(),
            'isLiked' => $post->isLiked(),
            'hasCommented' => true,
            'likesCount' => $post->likes()->count(),
            'sharesCount' => $post->shares()->count(),
            'commentsCount' => $post->comments()->count(),
            'createdAt' => $post->created_at,
            'comments' => $post->comments,
        ];
    }
}
