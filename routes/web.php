<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\FriendController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ReplyController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', [PostController::class, 'index'])->name('home');
Route::get('/home', [PostController::class, 'index'])->name('home');

Route::middleware('auth:web')->group(function () {
    Route::resource('users', UserController::class)->only(['index', 'show', 'update']);
    Route::put('users/{user}/banner', [UserController::class, 'addBanner']);
    Route::get('profile/edit', [ProfileController::class, 'edit']); //OK
    Route::put('profile/{profile}', [ProfileController::class, 'update']); //OK
    Route::put('profile/{profileId}/image', [ProfileController::class, 'upload']);
    Route::delete('/friends/{userSlug}', [FriendController::class, 'unfriend']);

    Route::get('posts', [PostController::class, 'show']);
    Route::post('posts/{post}/comments', [CommentController::class, 'store']);
    Route::post('comments/{comment}/reply', [ReplyController::class, 'store']);
    Route::get('comments/{comment}', [ReplyController::class, 'show']);
});
