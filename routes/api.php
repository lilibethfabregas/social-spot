<?php

use App\Http\Controllers\Api\CommentController;
use App\Http\Controllers\Api\PostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('posts', [PostController::class, 'getAll']);
Route::get('posts/post', [PostController::class, 'getOne']);
Route::get('profile-posts', [PostController::class, 'get']);
Route::post('posts/{post}/like', [PostController::class, 'like']);
Route::post('posts/{post}/share', [PostController::class, 'share']);
Route::get('post/{post}/comments', CommentController::class);
Route::post('posts', [PostController::class, 'store']);
Route::put('posts/{post}', [PostController::class, 'update']);
Route::delete('posts/{post}', [PostController::class, 'destroy']);
Route::get('comment/{comment}/reply', CommentController::class);
