# DR Social Spot

## Requirements

- PHP 7.4.*
- MySQL 8.0.*
- Node 13.x
- Yarn ^1.19

## Installation

Clone this repo:

```bash
git clone https://gitlab.com/BethLili/dr-social-spot.git
```

Install the composer dependencies:

```bash
composer install
```

Make a copy of the `.env.example` file into a `.env` file

```bash
cp .env.example .env
```

Once you have a `.env` file, configure all the necessary contents.

Once populated, run the following commands:

```bash
php artisan key:generate

php artisan migrate:fresh --seed

php artisan storage:link
```

You'll also need to compile the Javascript and CSS assets. Yarn is the preferred method here.

```bash
npm install

npm run dev
```
