import * as api from '../api'

export default {
    getPosts ({commit}) {
        api.getPosts(posts => {
            commit('setPosts', posts)
        })
    },
    getProfilePosts ({commit}) {
        api.getProfilePosts(posts => {
            commit('setProfilePosts', posts)
        })
    },
}
