export default {
    //triggered by submit button on personal info page
    setPosts(state, payload) {
        state.posts = payload
    },
    setProfilePosts(state, payload) {
        state.profilePosts = payload
    },
}
