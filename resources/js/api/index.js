export function getPosts(callback) {
    window.axios.get('/api/posts')
        .then(({data}) => {
            console.log('data from index.js')
            console.log(data)
            callback(data.data)
        }).catch(e => {
    })
}

export function getProfilePosts(callback) {
    window.axios.get('/api/profile-posts')
        .then(({data}) => {
            callback(data.data)
        }).catch(e => {

    })
}

