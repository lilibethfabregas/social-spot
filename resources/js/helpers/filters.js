import moment from 'moment'

export default {
    filters: {
        capitalize(value) {
            if (!value) return ''
            value = value.toString()
            return value.charAt(0).toUpperCase() + value.slice(1)
        },
        currency(value) {
            let formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'PHP',
            });

            return formatter.format(value);
        },
        number(value) {
            return value.toFixed(2);
        },
        date(value) {
            return moment(value).format('DD MMMM YYYY')
        },
        shortDate(value) {
            return moment(value).format('MM • DD • YY')
        },
        time(value) {
            return moment(value).format('h:s')
        },
        truncate(value) {
            if (value.length < 130) {
                return value
            } else {
                return value.slice(0, 130) + '...'
            }
        },
        truncate300(value) {
            if (value.length < 300) {
                return value
            } else {
                return value.slice(0, 300)
            }
        },
        truncate500(value) {
            if (value.length < 500) {
                return value
            } else {
                return value.slice(0, 500) + '...'
            }
        },
    },
}
