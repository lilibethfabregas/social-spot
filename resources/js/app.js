import store from './store'
require('./bootstrap');

window.Vue = require('vue');

Vue.component('list', require('./components/buttons/List.vue').default);
Vue.component('toggle', require('./components/buttons/Toggle.vue').default);

Vue.component('card-one', require('./components/cards/CardOne.vue').default);
Vue.component('card-biggest', require('./components/cards/CardBiggest.vue').default);
Vue.component('card-default', require('./components/cards/CardDefault.vue').default);
Vue.component('card-four', require('./components/cards/CardFour.vue').default);
Vue.component('card-five', require('./components/cards/CardFive.vue').default);
Vue.component('post-details', require('./components/cards/PostDetails.vue').default);

Vue.component('text-input', require('./components/TextInput.vue').default);

//actions and metrics
Vue.component('like', require('./components/likes/Like.vue').default); //OK
Vue.component('share', require('./components/shares/Share.vue').default); //OK
Vue.component('comment', require('./components/comments/Comment.vue').default); //OK

Vue.component('comments', require('./components/comments/Comments.vue').default);
Vue.component('comment-body', require('./components/comments/CommentBody.vue').default);
Vue.component('create-comment', require('./components/comments/Create.vue').default);

Vue.component('posts', require('./components/posts/Posts.vue').default);
Vue.component('profile-posts', require('./components/posts/ProfilePosts.vue').default); //OK
Vue.component('create-post', require('./components/posts/CreatePost.vue').default); //OK
Vue.component('all-post-details', require('./components/posts/AllPostDetails.vue').default);
Vue.component('edit-post', require('./components/posts/EditPost.vue').default);
Vue.component('delete-post', require('./components/posts/DeletePost.vue').default);
Vue.component('post', require('./components/posts/Post.vue').default); //USED. TODO UI

Vue.component('friend', require('./components/Friend.vue').default);


Vue.component('profile', require('./pages/Profile.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store
});


store.dispatch('getPosts')
store.dispatch('getProfilePosts')
