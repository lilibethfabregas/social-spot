@extends('layouts.app')

@section('content')
    @include('users._banner')
    @include('users._profile-photo')
    <div class="container mx-auto">
        <div class="w-full mx-auto relative">
            <div class="w-full h-card bg-two custom-shadow"
                 style="background: url({{ auth('web')->user()->getFirstMediaUrl('banner') }}) center center no-repeat;
                     background-size: cover">
            </div>

            <div class="absolute top-12 left-12 cursor-pointer" onclick="editBanner()">
                <i class="fas fa-camera fa-2x text-white"></i>
            </div>

            <div class="-mt-32 relative">
                <div class="absolute h-44 w-44 rounded-full -top-20 left-1/2 transform -translate-x-1/2"
                     style="background: url({{ auth('web')->user()->getImage() }}) center center no-repeat">
                    <div class="absolute bottom-4 right-4 cursor-pointer z-10 rounded-full bg-accent p-2" onclick="editProfilePhoto()">
                        <i class="fas fa-camera fa-2x text-white"></i>
                    </div>
                </div>

                <div class="bg-gray-50 mx-8 rounded-xl shadow-lg">
                    <div class="m-8">
                        <div class="pt-24 pb-2 text-center">
                            <h2 class="font-serif text-2xl">{{ auth('web')->user()->name }}</h2>
                                <p>Editing Profile</p>
                        </div>
                    </div>

                    <profile :profile="{{ auth('web')->user()->profile }}"></profile>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js_after')
    <script>
        function editBanner() {
            let bannerModal = document.getElementById("uploadBanner");
            bannerModal.classList.remove("hidden");
        }

        function editProfilePhoto () {
            let profilePhotoModal = document.getElementById("uploadProfilePhoto");
            profilePhotoModal.classList.remove("hidden");
        }
        function cancel() {
            let bannerModal = document.getElementById("uploadBanner");
            let profilePhotoModal = document.getElementById("uploadProfilePhoto");
            profilePhotoModal.classList.add("hidden");
            bannerModal.classList.add("hidden");
        }
    </script>
@endsection

