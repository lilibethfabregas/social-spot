@extends('layouts.app')

@section('content')
    @include('users._banner')
    @include('users._profile-photo')
    <div class="container mx-auto">
        <div class="w-full mx-auto relative">
            <div class="w-full h-card bg-two custom-shadow"
                 style="background: url({{ $user->getFirstMediaUrl('banner') }}) center center no-repeat;
                background-size: cover">
            </div>

            @if(auth('web')->id() === $user->id)
            <div class="absolute top-12 left-12 cursor-pointer" onclick="editBanner()">
                <i class="fas fa-camera fa-2x text-white"></i>
            </div>
            @endif

            <div class="-mt-32 relative">
                <div class="absolute h-44 w-44 rounded-full -top-20 left-1/2 transform -translate-x-1/2"
                     style="background: url({{ $user->getFirstMediaUrl('profile_photo') }}) center center no-repeat;
                         background-size: cover;">
                    @if(auth('web')->id() === $user->id)
                    <div class="absolute bottom-4 right-4 cursor-pointer z-10 rounded-full bg-accent p-2" onclick="editProfilePhoto()">
                        <i class="fas fa-camera fa-2x text-white"></i>
                    </div>
                    @endif
                </div>

                <div class="bg-gray-50 mx-8 rounded-xl shadow-sm">
                    <div class="m-8">
                        <div class="pt-24 pb-2 text-center">
                            <h2 class="font-serif text-2xl">{{ $profile->user->name }}</h2>
                            @if(auth('web')->id() === $user->id)
                                <p><a href="/profile/edit" class="underline cursor-pointer">
                                        Edit Profile
                                    </a></p>
                            @endif
                        </div>
                    </div>

                    <div class="m-8 shadow-sm rounded-lg bg-white px-4">
                        <div class="flex space-x-4">
                            <h2 class="font-serif text-2xl">Friends</h2>
                            <div class="underline pt-1"><a href="{{ route('users.index') }}">See all</a></div>
                        </div>

                        <div class="flex space-x-2 h-64 justify-center py-4">
                            @forelse(auth('web')->user()->friends->take(8) as $friend)
                                <div class="">
                                    <a href="/users/{{ $friend->slug }}">
                                        <div class="">
                                            <div class="h-36 w-36 rounded-lg"
                                                 style="background: url('{{ $friend->getFirstMediaUrl('profile_photo') }}') center center no-repeat;
                                                     background-size: cover">
                                            </div>
                                            <p class="text-center">{{ $friend->name }}</p>
                                        </div>
                                    </a>
                                </div>
                            @empty
                                <p>No friends. Awww.</p>
                            @endforelse
                        </div>
                    </div>

                    <div class="m-8 pb-12">
                        @if(auth('web')->id() === $user->id)
                            <create-post></create-post>
                        @endif

                        <profile-posts></profile-posts>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js_after')
    <script>
        function editBanner() {
            let bannerModal = document.getElementById("uploadBanner");
            bannerModal.classList.remove("hidden");
        }

        function editProfilePhoto () {
            let profilePhotoModal = document.getElementById("uploadProfilePhoto");
            profilePhotoModal.classList.remove("hidden");
        }
        function cancel() {
            let bannerModal = document.getElementById("uploadBanner");
            let profilePhotoModal = document.getElementById("uploadProfilePhoto");
            profilePhotoModal.classList.add("hidden");
            bannerModal.classList.add("hidden");
        }
    </script>
@endsection
