<div id="uploadBanner" class="hidden fixed z-10 inset-0 overflow-y-auto">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">

        <div class="fixed inset-0 transition-opacity" aria-hidden="true">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>

        <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

        <div
            class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                <form enctype="multipart/form-data" action="/users/{{auth('web')->user()->slug}}/banner" method="post">
                    @csrf @method('PUT')
                    <div class="">
                        <label for="banner">Banner</label>
                        <input type="file" class="form-control-file" id="banner" name="banner">
                    </div>
                    <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse" type="submit">
                        <button class="custom-btn w-20">
                            <span>Change</span>
                        </button>

                        <button class="custom-btn w-20" type="button">
                            <span onclick="cancel()">Cancel</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
