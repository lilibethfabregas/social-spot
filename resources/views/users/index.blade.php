@extends('layouts.app')

@section('content')

    @if(session()->has('success'))
        <div class="w-full md:w-8/12 mx-auto py-4">
            <p class="text-right text-gray-400 text-2xl font-cursive">
                <i class="far fa-check-circle"></i> {{ session()->get('success') }}
            </p>
        </div>
    @endif

    <div class="flex">
        <div class="w-1/12 h-screen relative">
            <div class="fixed top-1/2 transform -rotate-90">
                <div class="cstm-title text-9xl">FRIENDS</div>
            </div>
        </div>
        <div class="w-full md:w-8/12 mx-auto grid grid-cols-12 gap-2 gap-y-8 min-h-content h-card xl:h-64">
            @forelse ($friends as $friend)
                <friend :friend="{{ $friend }}" :image="'{{ $friend->getFirstMediaUrl('profile_photo') }}'"></friend>
            @empty
                <p>You have 0 friends. Awww.</p>
            @endforelse
        </div>
    </div>
@endsection
