@extends('layouts.app')

@section('content')
    <div class="w-full md:w-6/12 mx-auto md:py-20">

        <div class="w-full">
            <div class="cstm-title text-6xl md:text-7xl lg:text-9xl">REGISTER</div>
        </div>

        <div class="w-full md:w-10/12">
            <form method="POST" action="{{ route('register') }}">
                @csrf

                <x-input
                    name="name"
                    placeholder=" "
                    label="Name"
                    value="{{ old('name') }}"
                    type="name"
                ></x-input>

                <x-input
                    name="email"
                    placeholder=" "
                    label="Email"
                    value="{{ old('email') }}"
                    type="email"
                ></x-input>

                <x-input
                    name="password"
                    placeholder=" "
                    label="Password"
                    value="{{ old('password') }}"
                    type="password"
                ></x-input>

                <x-input
                    name="password_confirmation"
                    placeholder=" "
                    label="Confirm Password"
                    value="{{ old('password_confirmation') }}"
                    type="password"
                ></x-input>

                <div class="my-4">
                    <button type="submit" class="custom-btn w-40">
                        <span class="font-sans text-2xl">Register</span>
                    </button>
                </div>

            </form>
        </div>
    </div>
@endsection
