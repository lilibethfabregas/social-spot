<nav class="flex items-center justify-between flex-wrap bg-transparent text-gray-900 p-6 font-sans text-3xl">
    <a class="flex items-center flex-no-shrink mr-6 cursor-pointer fixed" href="/home">
        <span class="font-semibold name font-cursive">miniGram</span>
    </a>

    <div class="w-full flex-grow sm:flex sm:items-center sm:w-auto block justify-end">
        <div>
            <div class="sm:flex-grow md:space-x-6 flex">
                @guest
                    <li class="no-underline block mt-4 sm:inline-block sm:mt-0 font-sans hover:shadow-inner duration-300">
                        <a class="block px-8 py-2 focus:outline-none name font-serif"
                           href="{{ route('login') }}">Login</a>
                    </li>
                    <li class="no-underline block mt-4 sm:inline-block sm:mt-0 font-sans hover:shadow-inner duration-300">
                        <a class="block px-8 py-2 focus:outline-none name font-serif"
                           href="{{ route('register') }}">Register</a>
                    </li>

                @else
                    <li class="no-underline block mt-4 sm:inline-block sm:mt-0 font-sans hover:shadow-inner duration-300">
                        <a class="block px-8 py-2 focus:outline-none name font-serif"
                           href="{{  route('users.index') }}">Friends</a>
                    </li>

                    <li class="no-underline block mt-4 sm:inline-block sm:mt-0 font-sans hover:shadow-inner duration-300">
                        <a class="block px-8 py-2 focus:outline-none name font-serif"
                           href="{{ route('users.show', auth('web')->user()) }}">{{ auth('web')->user()->name }}</a>
                    </li>

                    <li class="no-underline block mt-4 sm:inline-block sm:mt-0 font-sans hover:shadow-inner duration-300">
                        <a class="block px-8 py-2 focus:outline-none name font-serif" href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                        <form id="logout-form"
                              action="{{ action([\App\Http\Controllers\Auth\LoginController::class, 'logout']) }}"
                              method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                @endguest
            </div>
        </div>
    </div>
</nav>
