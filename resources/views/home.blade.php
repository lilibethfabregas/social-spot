@extends('layouts.app')

@section('content')
    <div class="container mx-auto relative flex">

        <div class="w-1/12 h-screen">
            <div class="fixed top-1/2 transform -rotate-90">
                <div class="cstm-title text-9xl">POSTS</div>
            </div>
        </div>

        <div class="w-10/12 ml-auto">
            <posts></posts>
        </div>

    </div>
@endsection
